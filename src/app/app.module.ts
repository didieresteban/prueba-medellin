import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FooterComponent } from './templates/footer/footer.component';
import { HeaderComponent } from './templates/header/header.component';
import { ContentComponent } from './templates/content/content.component';
import { TravelService } from './templates/content/travel.service';
import { Http, HttpModule } from '@angular/http';
import { NgAutoCompleteModule } from "ng-auto-complete";
import { FormsModule } from '@angular/forms';
import { FilterComponent } from './templates/filter/filter.component';
import { Router,RouterModule } from '@angular/router';
import { FilterService } from './templates/filter/filter.service';


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    ContentComponent,
    FilterComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      { path: 'filter/:document', component: FilterComponent, pathMatch: 'full' },
      {path: 'content', component: ContentComponent, pathMatch: 'full'}
    ]),
    HttpModule,
    NgAutoCompleteModule,
    FormsModule
  ],
  providers: [TravelService, FilterService],
  bootstrap: [AppComponent]
})
export class AppModule {
 }
