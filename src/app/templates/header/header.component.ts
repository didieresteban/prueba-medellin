import { Component, OnInit } from '@angular/core';
import { FilterComponent } from '../filter/filter.component';
import { Router,RouterModule } from '@angular/router';

declare var jQuery:any;
declare var $:any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  responseServiceReservation: any[];
  responseService: any[];
  search : string;
  displayNone:string;
  private filterComponent: FilterComponent

  constructor(private router: Router) {
    this.router.navigate(['content']);
   }

  openModal(){
    $("#demo01").animatedModal();
  }

  ngOnInit() {
  }

  validateReservas() {
    this.router.navigate(['filter', this.search]);
  }
}
