import { Component, OnInit } from '@angular/core';
import { FilterService } from './filter.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {

  listFilterRe: any[];
  displayNone: string;
  tableStyle: string;
  constructor(private filterServices: FilterService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params
      .subscribe(params => {
        this.filterReservas(params.document);
      });
  }

  filterReservas(document) {
    this.filterServices.getDocument(document).map(res => res.json())
      // Subscribe to the observable to get the parsed people object and attach it to the
      // component
      .subscribe(result => {
        this.listFilterRe = result;
        console.log(this.listFilterRe);
      });

  }
}
