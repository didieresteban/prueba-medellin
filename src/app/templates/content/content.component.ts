import { Component, OnInit, ViewChild } from '@angular/core';
import { TravelService } from './travel.service';
import { NgForm } from '@angular/forms';
//componentes de form
import { FormControl, Validators } from '@angular/forms';
import { FilterService } from '../filter/filter.service';
declare var jQuery:any;
declare var $:any;

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  responseService: any[];

  responseServiceCyti: any[];

  number: number;

  urlImg: string;

  nameDestination: string;

  information: string;

  dataStart: string;

  dataEnd: string;

  cost: string;

  ContactModel = new ContactForm();

  constructor(private travelService: TravelService, private filterServices: FilterService) {
    this.findAllTravel();
    this.findCity();
  }

  ngOnInit() {
  }

  verMas(travel: any) {
    $('#modalTrigger').click();
    this.urlImg = travel.urlImg;
    this.nameDestination = travel.nameDestination;
    this.information = travel.information;
    this.number = travel.ratings;
    this.dataStart = travel.dataStart;
    this.dataEnd = travel.dataEnd;
    this.cost = travel.cost;
    if (this.number <= 1) {
      $("#uno").css("color", "#268420");
    } else if (this.number <= 2) {
      $("#uno").css("color", "#268420");
      $("#dos").css("color", "#268420");
    } else if (this.number <= 3) {
      $("#uno").css("color", "#268420");
      $("#dos").css("color", "#268420");
      $("#tres").css("color", "#268420");
    } else if (this.number <= 4) {
      $("#uno").css("color", "#268420");
      $("#dos").css("color", "#268420");
      $("#tres").css("color", "#268420");
      $("#cuatro").css("color", "#268420");
    } else if (this.number <= 5) {
      $("#uno").css("color", "#268420");
      $("#dos").css("color", "#268420");
      $("#tres").css("color", "#268420");
      $("#cuatro").css("color", "#268420");
      $("#cinco").css("color", "#268420");
    }
  }

  reservationModal() {
    $("#modalTriggerReservation").click();
  }

  findAllTravel() {
    let response = this.travelService.get().map(res => res.json())
      // Subscribe to the observable to get the parsed people object and attach it to the
      // component
      .subscribe(result => {
        this.responseService = result
      });
  }

  findCity() {
    let response = this.travelService.getCyti().map(res => res.json())
      // Subscribe to the observable to get the parsed people object and attach it to the
      // component
      .subscribe(result => {
        this.responseServiceCyti = result
      });
  }
  onSubmit(f: NgForm) {
    this.getSentServices(this.ContactModel, f);
  }

  getSentServices(body: ContactForm, f: NgForm) {
    this.travelService.reservationPost(body).subscribe(res => {
      alert("Se genero el viaje a nombre " + res.json().nameResident);
    }) 
  }
  

  CalculateAge() {
    let date = this.ContactModel.birthDate;
    let yeard = new Date(date);
    let yearAct = new Date();
    yeard.getFullYear();
    let cal = yearAct.getFullYear() - yeard.getFullYear();
    if (cal > 18) {

    } else {
      $("#alert").click();
    }
    //  let ed =(1 - parseInt(new Date(date))/365/24/60/60/1000);
  }

  validateTravell() {
    this.filterServices.getDocument(this.ContactModel.document).map(res => res.json())
      // Subscribe to the observable to get the parsed people object and attach it to the
      // component
      .subscribe(result => {
        if (result.length > 0) {
          for (let index = 0; index < result.length; index++) {
            const element = result[index];
            let data = element.dateCreate.split('T');
            let date = new Date().toISOString().slice(0,10);
            if(data[0] === date){
              $("#alertReservation").click();
            }
          }
        }
      });
  }

}


export class ContactForm {
  public nameResident: string;
  public document: string;
  public email: number;
  public birthDate: string;
  public halfPayment: string;
  public city: any;

}