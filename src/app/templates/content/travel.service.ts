import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map'

@Injectable()
export class TravelService {
  
  constructor(private http: Http) {
  }

  get() {
   return this.http.get('http://localhost:8180/travel/find_flights');
          // Call map on the response observable to get the parsed people object      
  }

  getCyti() {
    return this.http.get('http://localhost:8180/travel/city');
           // Call map on the response observable to get the parsed people object      
   }

   reservationPost(object: any) {
     console.log(object)
    let url = `http://localhost:8180/travel/reservation/save`;
    return this.http.post(url, object);
  }

}
